# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Shinjo Park <kde@peremen.name>, 2015, 2018, 2019, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-06-30 00:46+0000\n"
"PO-Revision-Date: 2021-05-16 21:26+0200\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean <kde-kr@kde.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 20.12.3\n"

#: package/contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "Appearance"
msgstr "모양"

#: package/contents/ui/configAppearance.qml:30
#, kde-format
msgid "%1pt"
msgstr "%1pt"

#: package/contents/ui/configAppearance.qml:36
#, kde-format
msgid "Text font size:"
msgstr "텍스트 글꼴 크기:"

#: package/contents/ui/configAppearance.qml:41
#, kde-format
msgid "Background color"
msgstr "배경색"

#: package/contents/ui/configAppearance.qml:77
#, kde-format
msgid "A white sticky note"
msgstr "흰색 메모지"

#: package/contents/ui/configAppearance.qml:78
#, kde-format
msgid "A black sticky note"
msgstr "검은색 메모지"

#: package/contents/ui/configAppearance.qml:79
#, kde-format
msgid "A red sticky note"
msgstr "빨간색 메모지"

#: package/contents/ui/configAppearance.qml:80
#, kde-format
msgid "An orange sticky note"
msgstr "주황색 메모지"

#: package/contents/ui/configAppearance.qml:81
#, kde-format
msgid "A yellow sticky note"
msgstr "노란색 메모지"

#: package/contents/ui/configAppearance.qml:82
#, kde-format
msgid "A green sticky note"
msgstr "녹색 메모지"

#: package/contents/ui/configAppearance.qml:83
#, kde-format
msgid "A blue sticky note"
msgstr "파란색 메모지"

#: package/contents/ui/configAppearance.qml:84
#, kde-format
msgid "A pink sticky note"
msgstr "분홍색 메모지"

#: package/contents/ui/configAppearance.qml:85
#, kde-format
msgid "A translucent sticky note"
msgstr "투명한 메모지"

#: package/contents/ui/configAppearance.qml:86
#, kde-format
msgid "A translucent sticky note with light text"
msgstr "밝은 텍스트가 있는 반투명한 메모지"

#: package/contents/ui/main.qml:246
#, kde-format
msgid "Undo"
msgstr "실행 취소"

#: package/contents/ui/main.qml:254
#, kde-format
msgid "Redo"
msgstr "다시 실행"

#: package/contents/ui/main.qml:264
#, kde-format
msgid "Cut"
msgstr "잘라내기"

#: package/contents/ui/main.qml:272
#, kde-format
msgid "Copy"
msgstr "복사"

#: package/contents/ui/main.qml:280
#, kde-format
msgid "Paste Without Formatting"
msgstr "서식 없이 붙여넣기"

#: package/contents/ui/main.qml:286
#, kde-format
msgid "Paste"
msgstr "붙여넣기"

#: package/contents/ui/main.qml:295
#, kde-format
msgid "Delete"
msgstr "삭제"

#: package/contents/ui/main.qml:302
#, kde-format
msgid "Clear"
msgstr "지우기"

#: package/contents/ui/main.qml:312
#, kde-format
msgid "Select All"
msgstr "모두 선택"

#: package/contents/ui/main.qml:440
#, kde-format
msgctxt "@info:tooltip"
msgid "Bold"
msgstr "굵게"

#: package/contents/ui/main.qml:452
#, kde-format
msgctxt "@info:tooltip"
msgid "Italic"
msgstr "이탤릭"

#: package/contents/ui/main.qml:464
#, kde-format
msgctxt "@info:tooltip"
msgid "Underline"
msgstr "밑줄"

#: package/contents/ui/main.qml:476
#, kde-format
msgctxt "@info:tooltip"
msgid "Strikethrough"
msgstr "취소선"

#: package/contents/ui/main.qml:550
#, kde-format
msgid "Discard this note?"
msgstr "이 메모를 삭제하시겠습니까?"

#: package/contents/ui/main.qml:551
#, kde-format
msgid "Are you sure you want to discard this note?"
msgstr "이 메모를 삭제하시겠습니까?"

#: package/contents/ui/main.qml:564
#, kde-format
msgctxt "@item:inmenu"
msgid "White"
msgstr "흰색"

#: package/contents/ui/main.qml:565
#, kde-format
msgctxt "@item:inmenu"
msgid "Black"
msgstr "검은색"

#: package/contents/ui/main.qml:566
#, kde-format
msgctxt "@item:inmenu"
msgid "Red"
msgstr "빨간색"

#: package/contents/ui/main.qml:567
#, kde-format
msgctxt "@item:inmenu"
msgid "Orange"
msgstr "주황색"

#: package/contents/ui/main.qml:568
#, kde-format
msgctxt "@item:inmenu"
msgid "Yellow"
msgstr "노란색"

#: package/contents/ui/main.qml:569
#, kde-format
msgctxt "@item:inmenu"
msgid "Green"
msgstr "녹색"

#: package/contents/ui/main.qml:570
#, kde-format
msgctxt "@item:inmenu"
msgid "Blue"
msgstr "파란색"

#: package/contents/ui/main.qml:571
#, kde-format
msgctxt "@item:inmenu"
msgid "Pink"
msgstr "분홍색"

#: package/contents/ui/main.qml:572
#, kde-format
msgctxt "@item:inmenu"
msgid "Translucent"
msgstr "투명"

#: package/contents/ui/main.qml:573
#, kde-format
msgctxt "@item:inmenu"
msgid "Translucent Light"
msgstr "투명한 조명"

#~ msgid "Toggle text format options"
#~ msgstr "텍스트 서식 옵션 전환"

#~ msgid "Notes Settings..."
#~ msgstr "메모 설정..."

#~ msgid "Align left"
#~ msgstr "왼쪽 정렬"

#~ msgid "Align center"
#~ msgstr "가운데 정렬"

#~ msgid "Align right"
#~ msgstr "오른쪽 정렬"

#~ msgid "Justified"
#~ msgstr "양쪽 맞춤"

#~ msgid "Font"
#~ msgstr "글꼴:"

#~ msgid "Style:"
#~ msgstr "스타일:"

#~ msgid "&Bold"
#~ msgstr "진하게(&B)"

#~ msgid "&Italic"
#~ msgstr "기울임꼴(&I)"

#~ msgid "Size:"
#~ msgstr "크기:"

#~ msgid "Scale font size by:"
#~ msgstr "글꼴 크기 조정:"

#~ msgid "%"
#~ msgstr "%"

#~ msgid "Color:"
#~ msgstr "색:"

#~ msgid "Use theme color"
#~ msgstr "테마 색 사용하기"

#~ msgid "Use custom color:"
#~ msgstr "사용자 정의 색 사용하기:"

#~ msgid "Active line highlight color:"
#~ msgstr "현재 줄 강조색:"

#~ msgid "Use no color"
#~ msgstr "색 사용하지 않기"

#~ msgid "Theme"
#~ msgstr "테마"

#~ msgid "Notes color:"
#~ msgstr "메모 색:"

#~ msgid "Spell Check"
#~ msgstr "맞춤법 검사"

#~ msgid "Enable spell check:"
#~ msgstr "맞춤법 검사 사용하기:"

#~ msgid "Notes Color"
#~ msgstr "메모 색"

#~ msgid "General"
#~ msgstr "일반"

#~ msgid "Formatting"
#~ msgstr "서식"
