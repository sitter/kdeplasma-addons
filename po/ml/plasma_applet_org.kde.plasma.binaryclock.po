# Malayalam translations for kdeplasma-addons package.
# Copyright (C) 2019 This file is copyright:
# This file is distributed under the same license as the kdeplasma-addons package.
# Automatically generated, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: kdeplasma-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2020-09-25 02:25+0200\n"
"PO-Revision-Date: 2019-03-14 03:29+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Swathanthra|സ്വതന്ത്ര Malayalam|മലയാളം Computing|കമ്പ്യൂട്ടിങ്ങ് <smc."
"org.in>\n"
"Language: ml\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: package/contents/config/config.qml:17
#, kde-format
msgctxt "@title"
msgid "Appearance"
msgstr ""

#: package/contents/ui/configGeneral.qml:35
#, kde-format
msgid "Display:"
msgstr ""

#: package/contents/ui/configGeneral.qml:36
#: package/contents/ui/configGeneral.qml:90
#, kde-format
msgctxt "@option:check"
msgid "Grid"
msgstr ""

#: package/contents/ui/configGeneral.qml:41
#: package/contents/ui/configGeneral.qml:77
#, kde-format
msgctxt "@option:check"
msgid "Inactive LEDs"
msgstr ""

#: package/contents/ui/configGeneral.qml:46
#, kde-format
msgctxt "@option:check"
msgid "Seconds"
msgstr ""

#: package/contents/ui/configGeneral.qml:51
#, kde-format
msgctxt "@option:check"
msgid "In BCD format (decimal)"
msgstr ""

#: package/contents/ui/configGeneral.qml:60
#, kde-format
msgid "Use custom color for:"
msgstr ""

#: package/contents/ui/configGeneral.qml:64
#, kde-format
msgctxt "@option:check"
msgid "Active LEDs"
msgstr ""
