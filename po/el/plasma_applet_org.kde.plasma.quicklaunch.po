# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Dimitris Kardarakos <dimkard@gmail.com>, 2015, 2016.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-15 00:46+0000\n"
"PO-Revision-Date: 2016-02-07 11:57+0200\n"
"Last-Translator: Dimitris Kardarakos <dimkard@gmail.com>\n"
"Language-Team: Greek <kde-i18n-el@kde.org>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: package/contents/config/config.qml:12
#, fuzzy, kde-format
#| msgid "General"
msgctxt "@title"
msgid "General"
msgstr "Γενικά"

#: package/contents/ui/ConfigGeneral.qml:30
#, fuzzy, kde-format
#| msgid "Maximum columns:"
msgctxt "@label:spinbox"
msgid "Maximum columns:"
msgstr "Μέγιστος αριθμός στηλών:"

#: package/contents/ui/ConfigGeneral.qml:30
#, fuzzy, kde-format
#| msgid "Maximum rows:"
msgctxt "@label:spinbox"
msgid "Maximum rows:"
msgstr "Μέγιστος αριθμός γραμμών:"

#: package/contents/ui/ConfigGeneral.qml:44
#, fuzzy, kde-format
#| msgid "Appearance"
msgctxt "@title:group"
msgid "Appearance:"
msgstr "Εμφάνιση"

#: package/contents/ui/ConfigGeneral.qml:46
#, fuzzy, kde-format
#| msgid "Show launcher names"
msgctxt "@option:check"
msgid "Show launcher names"
msgstr "Εμφάνιση ονομάτων εκτελεστών"

#: package/contents/ui/ConfigGeneral.qml:51
#, fuzzy, kde-format
#| msgid "Enable popup"
msgctxt "@option:check"
msgid "Enable popup"
msgstr "Ενεργοποίηση αναδυόμενου"

#: package/contents/ui/ConfigGeneral.qml:61
#, fuzzy, kde-format
#| msgid "Title"
msgctxt "@title:group"
msgid "Title:"
msgstr "Τίτλος"

#: package/contents/ui/ConfigGeneral.qml:69
#, kde-format
msgctxt "@option:check"
msgid "Show:"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:85
#, fuzzy, kde-format
#| msgid "Show title"
msgctxt "@info:placeholder"
msgid "Custom title"
msgstr "Εμφάνιση τίτλου"

#: package/contents/ui/IconItem.qml:175
#, kde-format
msgid "Launch %1"
msgstr ""

#: package/contents/ui/IconItem.qml:259
#, fuzzy, kde-format
#| msgid "Add Launcher..."
msgctxt "@action:inmenu"
msgid "Add Launcher…"
msgstr "Προσθήκη εκτελεστή..."

#: package/contents/ui/IconItem.qml:265
#, fuzzy, kde-format
#| msgid "Edit Launcher..."
msgctxt "@action:inmenu"
msgid "Edit Launcher…"
msgstr "Επεξεργασία εκτελεστή..."

#: package/contents/ui/IconItem.qml:271
#, fuzzy, kde-format
#| msgid "Remove Launcher"
msgctxt "@action:inmenu"
msgid "Remove Launcher"
msgstr "Αφαίρεση εκτελεστή"

#: package/contents/ui/main.qml:142
#, kde-format
msgid "Quicklaunch"
msgstr "Γρήγορη εκτέλεση"

#: package/contents/ui/main.qml:143
#, fuzzy, kde-format
#| msgid "Add launchers by Drag and Drop or by using the context menu."
msgctxt "@info"
msgid "Add launchers by Drag and Drop or by using the context menu."
msgstr ""
"Προσθέστε εκτελεστές με σύρσιμο και απόθεση ή χρησιμοποιώντας το βοηθητικό "
"μενού."

#: package/contents/ui/main.qml:171
#, kde-format
msgid "Hide icons"
msgstr "Απόκρυψη εικονιδίων"

#: package/contents/ui/main.qml:171
#, kde-format
msgid "Show hidden icons"
msgstr "Εμφάνιση κρυφών εικονιδίων"

#: package/contents/ui/main.qml:285
#, fuzzy, kde-format
#| msgid "Add Launcher..."
msgctxt "@action"
msgid "Add Launcher…"
msgstr "Προσθήκη εκτελεστή..."

#, fuzzy
#~| msgid "Arrangement"
#~ msgctxt "@title:group"
#~ msgid "Arrangement"
#~ msgstr "Διάταξη"

#, fuzzy
#~| msgid "Enter title"
#~ msgctxt "@info:placeholder"
#~ msgid "Enter title"
#~ msgstr "Εισαγωγή τίτλου"
