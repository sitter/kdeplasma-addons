msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-02-03 00:45+0000\n"
"PO-Revision-Date: 2022-10-02 15:49\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/kdeplasma-addons/plasma_applet_org.kde."
"plasma.quickshare.pot\n"
"X-Crowdin-File-ID: 4544\n"

#: contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "General"
msgstr "常规"

#: contents/ui/main.qml:225
#, kde-format
msgctxt "@action"
msgid "Paste"
msgstr "粘贴"

#: contents/ui/main.qml:253 contents/ui/main.qml:293
#, kde-format
msgid "Share"
msgstr "分享"

#: contents/ui/main.qml:254 contents/ui/main.qml:294
#, kde-format
msgid "Drop text or an image onto me to upload it to an online service."
msgstr "拖放文本或图像到此处，即可上传到在线服务。"

#: contents/ui/main.qml:294
#, kde-format
msgid "Upload %1 to an online service"
msgstr "上传 %1 到在线服务"

#: contents/ui/main.qml:307
#, kde-format
msgid "Sending…"
msgstr "正在发送…"

#: contents/ui/main.qml:308
#, kde-format
msgid "Please wait"
msgstr "请稍候"

#: contents/ui/main.qml:315
#, kde-format
msgid "Successfully uploaded"
msgstr "上传成功"

#: contents/ui/main.qml:316
#, kde-format
msgid "<a href='%1'>%1</a>"
msgstr "<a href='%1'>%1</a>"

#: contents/ui/main.qml:323
#, kde-format
msgid "Error during upload."
msgstr "上传时发生错误。"

#: contents/ui/main.qml:324
#, kde-format
msgid "Please, try again."
msgstr "请重试。"

#: contents/ui/settingsGeneral.qml:21
#, kde-format
msgctxt "@label:spinbox"
msgid "History size:"
msgstr "历史大小："

#: contents/ui/settingsGeneral.qml:31
#, kde-format
msgctxt "@option:check"
msgid "Copy automatically:"
msgstr "自动复制："

#: contents/ui/ShareDialog.qml:31
#, kde-format
msgid "Shares for '%1'"
msgstr "分享“%1”"

#: contents/ui/ShowUrlDialog.qml:42
#, kde-format
msgid "The URL was just shared"
msgstr "该 URL 刚才已被分享"

#: contents/ui/ShowUrlDialog.qml:48
#, kde-format
msgctxt "@option:check"
msgid "Don't show this dialog, copy automatically."
msgstr "不显示此对话框，自动复制。"

#: contents/ui/ShowUrlDialog.qml:56
#, kde-format
msgctxt "@action:button"
msgid "Close"
msgstr "关闭"
