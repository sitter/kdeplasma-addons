msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-18 00:46+0000\n"
"PO-Revision-Date: 2022-10-02 15:49\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/kdeplasma-addons/plasma_applet_org.kde."
"plasma.colorpicker.pot\n"
"X-Crowdin-File-ID: 4128\n"

#: package/contents/config/config.qml:11
#, kde-format
msgctxt "@title"
msgid "General"
msgstr "常规"

#: package/contents/ui/configGeneral.qml:24
#, kde-format
msgctxt "@label:listbox"
msgid "Default color format:"
msgstr "默认颜色格式："

#: package/contents/ui/configGeneral.qml:32
#, kde-format
msgctxt "@option:check"
msgid "Automatically copy color to clipboard"
msgstr "自动复制颜色到剪贴板"

#: package/contents/ui/configGeneral.qml:40
#, kde-format
msgctxt "@label"
msgid "When pressing the keyboard shortcut:"
msgstr "按下键盘快捷键时："

#: package/contents/ui/configGeneral.qml:41
#, kde-format
msgctxt "@option:radio"
msgid "Pick a color"
msgstr "拾取颜色"

#: package/contents/ui/configGeneral.qml:47
#, kde-format
msgctxt "@option:radio"
msgid "Show history"
msgstr "显示历史"

#: package/contents/ui/logic.js:58
#, kde-format
msgctxt "@title:menu"
msgid "Copy to Clipboard"
msgstr "复制到剪贴板"

#: package/contents/ui/main.qml:105
#, kde-format
msgctxt "@action"
msgid "Open Color Dialog"
msgstr "打开颜色对话框"

#: package/contents/ui/main.qml:106
#, kde-format
msgctxt "@action"
msgid "Clear History"
msgstr "清除历史"

#: package/contents/ui/main.qml:124
#, kde-format
msgctxt "@info:tooltip"
msgid "Pick color"
msgstr "拾取颜色"

#: package/contents/ui/main.qml:199
#, kde-format
msgctxt "@info:usagetip"
msgid "No colors"
msgstr ""

#: package/contents/ui/main.qml:203
#, kde-format
msgctxt "@action:button"
msgid "Pick Color"
msgstr "拾取颜色"

#: package/contents/ui/main.qml:370
#, kde-format
msgctxt "@info:progress just copied a color to clipboard"
msgid "Copied!"
msgstr ""

#: package/contents/ui/main.qml:390
#, kde-format
msgctxt "@action:button"
msgid "Delete"
msgstr ""

#~ msgctxt "@info:tooltip"
#~ msgid "Color options"
#~ msgstr "颜色选项"
