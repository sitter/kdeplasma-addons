# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Alexander Potashev <aspotashev@gmail.com>, 2018.
# Alexander Yavorsky <kekcuha@gmail.com>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-07-31 00:48+0000\n"
"PO-Revision-Date: 2022-08-13 15:27+0300\n"
"Last-Translator: Alexander Yavorsky <kekcuha@gmail.com>\n"
"Language-Team: Russian <kde-russian@lists.kde.ru>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 22.04.3\n"

#: package/contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "Dictionaries"
msgstr "Словари"

#: package/contents/ui/AvailableDictSheet.qml:35
#, kde-format
msgid "Add More Dictionaries"
msgstr "Добавить словари"

#: package/contents/ui/ConfigDictionaries.qml:89
#, kde-format
msgid "Unable to load dictionary list"
msgstr "Не удалось загрузить список словарей"

#: package/contents/ui/ConfigDictionaries.qml:90
#: package/contents/ui/main.qml:121
#, kde-format
msgctxt "%2 human-readable error string"
msgid "Error code: %1 (%2)"
msgstr "Error code: %1 (%2)"

#: package/contents/ui/ConfigDictionaries.qml:101
#, kde-format
msgid "No dictionaries"
msgstr "Словари отсутствуют"

#: package/contents/ui/ConfigDictionaries.qml:112
#, kde-format
msgid "Add More…"
msgstr "Добавить…"

#: package/contents/ui/DictItemDelegate.qml:51
#, kde-format
msgid "Delete"
msgstr "Удалить"

#: package/contents/ui/main.qml:40
#, kde-format
msgctxt "@info:placeholder"
msgid "Enter word to define here…"
msgstr "Введите термин…"

#: package/contents/ui/main.qml:120
#, kde-format
msgid "Unable to load definition"
msgstr "Не удалось загрузить определение"

#~ msgid "Looking up definition…"
#~ msgstr "Поиск толкования…"
