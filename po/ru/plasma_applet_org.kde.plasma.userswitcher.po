# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Alexander Potashev <aspotashev@gmail.com>, 2016, 2018, 2019.
# Alexander Yavorsky <kekcuha@gmail.com>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-10 00:46+0000\n"
"PO-Revision-Date: 2022-09-23 22:02+0300\n"
"Last-Translator: Alexander Yavorsky <kekcuha@gmail.com>\n"
"Language-Team: Russian <kde-russian@lists.kde.ru>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 21.08.3\n"

#: package/contents/config/config.qml:11
#, kde-format
msgctxt "@title"
msgid "General"
msgstr "Основное"

#: package/contents/ui/configGeneral.qml:27
#, kde-format
msgctxt "@title:label"
msgid "Username style:"
msgstr "Вид имени пользователя:"

#: package/contents/ui/configGeneral.qml:30
#, kde-format
msgctxt "@option:radio"
msgid "Full name (if available)"
msgstr "Полное имя (если указано)"

#: package/contents/ui/configGeneral.qml:37
#, kde-format
msgctxt "@option:radio"
msgid "Login username"
msgstr "Имя пользователя, используемое для входа в систему"

#: package/contents/ui/configGeneral.qml:55
#, kde-format
msgctxt "@title:label"
msgid "Show:"
msgstr "Показывать:"

#: package/contents/ui/configGeneral.qml:58
#, kde-format
msgctxt "@option:radio"
msgid "Name"
msgstr "Имя"

#: package/contents/ui/configGeneral.qml:72
#, kde-format
msgctxt "@option:radio"
msgid "User picture"
msgstr "Изображение пользователя"

#: package/contents/ui/configGeneral.qml:86
#, kde-format
msgctxt "@option:radio"
msgid "Name and user picture"
msgstr "Имя и изображение пользователя"

#: package/contents/ui/configGeneral.qml:105
#, kde-format
msgctxt "@title:label"
msgid "Advanced:"
msgstr "Дополнительно:"

#: package/contents/ui/configGeneral.qml:107
#, kde-format
msgctxt "@option:check"
msgid "Show technical session information"
msgstr "Показывать техническую информацию о сеансах"

#: package/contents/ui/main.qml:39
#, kde-format
msgid "You are logged in as <b>%1</b>"
msgstr "Вход выполнен от имени <b>%1</b>"

#: package/contents/ui/main.qml:134
#, kde-format
msgid "Current user"
msgstr "Текущий пользователь"

# BUGME: this string is duplicate against plasma_lookandfeel_org.kde.lookandfeel.po --aspotashev
#: package/contents/ui/main.qml:165
#, kde-format
msgctxt "Nobody logged in on that session"
msgid "Unused"
msgstr "Не используется"

# BUGME: this string is duplicate against plasma_lookandfeel_org.kde.lookandfeel.po --aspotashev
#: package/contents/ui/main.qml:181
#, kde-format
msgctxt "User logged in on console number"
msgid "TTY %1"
msgstr "Терминал %1"

# BUGME: this string is duplicate against plasma_lookandfeel_org.kde.lookandfeel.po --aspotashev
#: package/contents/ui/main.qml:183
#, kde-format
msgctxt "User logged in on console (X display number)"
msgid "on %1 (%2)"
msgstr "на %1 (%2)"

#: package/contents/ui/main.qml:191
#, kde-format
msgctxt "@action:button"
msgid "Switch to User %1"
msgstr "Переключить на пользователя %1"

#: package/contents/ui/main.qml:200
#, kde-format
msgctxt "@action"
msgid "New Session"
msgstr "Начать новый сеанс"

#: package/contents/ui/main.qml:212
#, kde-format
msgctxt "@action"
msgid "Lock Screen"
msgstr "Заблокировать экран"

#: package/contents/ui/main.qml:224
#, kde-format
msgctxt "Show a dialog with options to logout/shutdown/restart"
msgid "Log Out"
msgstr "Завершить сеанс"

#~ msgctxt "Show a dialog with options to logout/shutdown/restart"
#~ msgid "Leave…"
#~ msgstr "Завершить работу…"

#~ msgctxt "@title:group"
#~ msgid "Layout"
#~ msgstr "Внешний вид"

#~ msgctxt "@option:radio"
#~ msgid "Show only name"
#~ msgstr "Показывать только имя"

#~ msgctxt "@option:radio"
#~ msgid "Show only avatar"
#~ msgstr "Показывать только значок пользователя"

#~ msgctxt "@option:radio"
#~ msgid "Show both avatar and name"
#~ msgstr "Показывать имя и значок пользователя"
