# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Alexander Potashev <aspotashev@gmail.com>, 2018.
# Alexander Yavorsky <kekcuha@gmail.com>, 2018, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-08-01 00:45+0000\n"
"PO-Revision-Date: 2022-05-18 12:36+0300\n"
"Last-Translator: Alexander Yavorsky <kekcuha@gmail.com>\n"
"Language-Team: Russian <kde-russian@lists.kde.ru>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 21.08.3\n"

#: contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "Keys"
msgstr "Клавиши"

#: contents/ui/configAppearance.qml:33
#, kde-format
msgctxt ""
"@label show keyboard indicator when Caps Lock or Num Lock is activated"
msgid "Show when activated:"
msgstr "Показывать когда включён:"

#: contents/ui/configAppearance.qml:36
#, kde-format
msgctxt "@option:check"
msgid "Caps Lock"
msgstr "Caps Lock"

#: contents/ui/configAppearance.qml:43
#, kde-format
msgctxt "@option:check"
msgid "Num Lock"
msgstr "Num Lock"

#: contents/ui/main.qml:29
#, kde-format
msgid "Caps Lock activated\n"
msgstr "Caps Lock включён\n"

#: contents/ui/main.qml:30
#, kde-format
msgid "Num Lock activated\n"
msgstr "Num Lock включён\n"

#: contents/ui/main.qml:108
#, kde-format
msgid "No lock keys activated"
msgstr "Caps Lock и Num Lock отключены"

#~ msgid "Num Lock"
#~ msgstr "Num Lock"

#~ msgid "%1: Locked\n"
#~ msgstr "%1: включено\n"

#~ msgid "Unlocked"
#~ msgstr "Отключено"
