# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdeplasma-addons package.
#
# Kheyyam Gojayev <xxmn77@gmail.com>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kdeplasma-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-28 00:51+0000\n"
"PO-Revision-Date: 2022-08-24 17:51+0400\n"
"Last-Translator: Kheyyam <xxmn77@gmail.com>\n"
"Language-Team: Azerbaijani <kde-i18n-doc@kde.org>\n"
"Language: az\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.08.0\n"

#: package/contents/ui/config.qml:48
#, kde-format
msgctxt "@label:listbox"
msgid "Provider:"
msgstr "Təminatçı:"

#: package/contents/ui/config.qml:66
#, kde-format
msgctxt "@option:check"
msgid "Update when using metered network connection"
msgstr "Limitli internet bağlantısı zamanı yeniləmək"

#: package/contents/ui/config.qml:76
#, kde-format
msgctxt "@label:listbox"
msgid "Category:"
msgstr "Kateqoriya:"

#: package/contents/ui/config.qml:80
#, kde-format
msgctxt "@item:inlistbox"
msgid "All"
msgstr "Bütün"

#: package/contents/ui/config.qml:84
#, kde-format
msgctxt "@item:inlistbox"
msgid "1080p"
msgstr "1080p"

#: package/contents/ui/config.qml:88
#, kde-format
msgctxt "@item:inlistbox"
msgid "4K"
msgstr "4K"

#: package/contents/ui/config.qml:92
#, kde-format
msgctxt "@item:inlistbox"
msgid "Ultra Wide"
msgstr "Ultra geniş"

#: package/contents/ui/config.qml:96
#, kde-format
msgctxt "@item:inlistbox"
msgid "Background"
msgstr "Arxa fon"

#: package/contents/ui/config.qml:100
#, kde-format
msgctxt "@item:inlistbox"
msgid "Lock Screen"
msgstr "Ekranı kilidləmək"

#: package/contents/ui/config.qml:104
#, kde-format
msgctxt "@item:inlistbox"
msgid "Nature"
msgstr "Təbiət"

#: package/contents/ui/config.qml:108
#, kde-format
msgctxt "@item:inlistbox"
msgid "Tumblr"
msgstr "Tumblr"

#: package/contents/ui/config.qml:112
#, kde-format
msgctxt "@item:inlistbox"
msgid "Black"
msgstr "Qara"

#: package/contents/ui/config.qml:116
#, kde-format
msgctxt "@item:inlistbox"
msgid "Flower"
msgstr "Çiçək"

#: package/contents/ui/config.qml:120
#, kde-format
msgctxt "@item:inlistbox"
msgid "Funny"
msgstr "Əyləncəli"

#: package/contents/ui/config.qml:124
#, kde-format
msgctxt "@item:inlistbox"
msgid "Cute"
msgstr "Xoş"

#: package/contents/ui/config.qml:128
#, kde-format
msgctxt "@item:inlistbox"
msgid "Cool"
msgstr "Soyuq"

#: package/contents/ui/config.qml:132
#, kde-format
msgctxt "@item:inlistbox"
msgid "Fall"
msgstr "Payız"

#: package/contents/ui/config.qml:136
#, kde-format
msgctxt "@item:inlistbox"
msgid "Love"
msgstr "Sevgi"

#: package/contents/ui/config.qml:140
#, kde-format
msgctxt "@item:inlistbox"
msgid "Design"
msgstr "Dizayn"

#: package/contents/ui/config.qml:144
#, kde-format
msgctxt "@item:inlistbox"
msgid "Christmas"
msgstr "Milad"

#: package/contents/ui/config.qml:148
#, kde-format
msgctxt "@item:inlistbox"
msgid "Travel"
msgstr "Səyahət"

#: package/contents/ui/config.qml:152
#, kde-format
msgctxt "@item:inlistbox"
msgid "Beach"
msgstr "Çimərlik"

#: package/contents/ui/config.qml:156
#, kde-format
msgctxt "@item:inlistbox"
msgid "Car"
msgstr "Avtomobil"

#: package/contents/ui/config.qml:160
#, kde-format
msgctxt "@item:inlistbox"
msgid "Sports"
msgstr "İdman"

#: package/contents/ui/config.qml:164
#, kde-format
msgctxt "@item:inlistbox"
msgid "Animal"
msgstr "Heyvanat"

#: package/contents/ui/config.qml:168
#, kde-format
msgctxt "@item:inlistbox"
msgid "People"
msgstr "Adamlar"

#: package/contents/ui/config.qml:172
#, kde-format
msgctxt "@item:inlistbox"
msgid "Music"
msgstr "Musiqi"

#: package/contents/ui/config.qml:176
#, kde-format
msgctxt "@item:inlistbox"
msgid "Summer"
msgstr "Yay"

#: package/contents/ui/config.qml:180
#, kde-format
msgctxt "@item:inlistbox"
msgid "Galaxy"
msgstr "Qalaktika"

#: package/contents/ui/config.qml:184
#, kde-format
msgctxt "@item:inlistbox"
msgid "Tabliss"
msgstr "Tabliss"

#: package/contents/ui/config.qml:214
#, kde-format
msgctxt "@label:listbox"
msgid "Positioning:"
msgstr "Yerləşdirmə:"

#: package/contents/ui/config.qml:217
#, kde-format
msgctxt "@item:inlistbox"
msgid "Scaled and Cropped"
msgstr "Miqyaslandırılmış və Kəsilmiş"

#: package/contents/ui/config.qml:221
#, kde-format
msgctxt "@item:inlistbox"
msgid "Scaled"
msgstr "Tam İş masası boyu"

#: package/contents/ui/config.qml:225
#, kde-format
msgctxt "@item:inlistbox"
msgid "Scaled, Keep Proportions"
msgstr "Nisbətləri saxlanılmaqla miqyaslanmış"

#: package/contents/ui/config.qml:229
#, kde-format
msgctxt "@item:inlistbox"
msgid "Centered"
msgstr "Mərkəzdə"

#: package/contents/ui/config.qml:233
#, kde-format
msgctxt "@item:inlistbox"
msgid "Tiled"
msgstr "Yan-yana"

#: package/contents/ui/config.qml:254
#, kde-format
msgctxt "@label:chooser"
msgid "Background color:"
msgstr "Fon Rəngi:"

#: package/contents/ui/config.qml:255
#, kde-format
msgctxt "@title:window"
msgid "Select Background Color"
msgstr "Arxa fon rəngini seçmək"

#: package/contents/ui/config.qml:266
#, kde-format
msgctxt "@label"
msgid "Today's picture:"
msgstr "Bu günün şəkli:"

#: package/contents/ui/config.qml:283
#, kde-format
msgctxt "@label"
msgid "Title:"
msgstr "Başlıq:"

#: package/contents/ui/config.qml:303
#, kde-format
msgctxt "@label"
msgid "Author:"
msgstr "Müəllif:"

#: package/contents/ui/config.qml:329
#, kde-format
msgctxt "@action:button"
msgid "Open Containing Folder"
msgstr "Bu fayl olan qovluğu açın"

#: package/contents/ui/config.qml:333
#, kde-format
msgctxt "@info:whatsthis for a button"
msgid "Open the destination folder where the wallpaper image was saved."
msgstr "Divar kağızı şəkli saxlanılan qovluğu açın"

#: package/contents/ui/WallpaperDelegate.qml:142
#, kde-format
msgctxt "@info:whatsthis"
msgid "Today's picture"
msgstr "Bu günün şəkli"

#: package/contents/ui/WallpaperDelegate.qml:143
#, kde-format
msgctxt "@info:whatsthis"
msgid "Loading"
msgstr "Yüklənir"

#: package/contents/ui/WallpaperDelegate.qml:144
#, kde-format
msgctxt "@info:whatsthis"
msgid "Unavailable"
msgstr "Əlçatmaz"

#: package/contents/ui/WallpaperDelegate.qml:145
#, kde-format
msgctxt "@info:whatsthis for an image %1 title %2 author"
msgid "%1 Author: %2. Right-click on the image to see more actions."
msgstr "%1 müəllif: %2 daha çıx əməlləri görmək üçün şəkilə vurun."

#: package/contents/ui/WallpaperDelegate.qml:146
#, kde-format
msgctxt "@info:whatsthis"
msgid "The wallpaper is being fetched from the Internet."
msgstr "Divar kağızı internetdən yüklənir."

#: package/contents/ui/WallpaperDelegate.qml:147
#, kde-format
msgctxt "@info:whatsthis"
msgid "Failed to fetch the wallpaper from the Internet."
msgstr "Divar kağızının internetdən yüklənməsi uğursuz oldu."

#: package/contents/ui/WallpaperPreview.qml:50
#, kde-format
msgctxt "@action:inmenu wallpaper preview menu"
msgid "Save Image as…"
msgstr "Şəkili belə sazlayın..."

#: package/contents/ui/WallpaperPreview.qml:53
#, kde-format
msgctxt "@info:whatsthis for a button and a menu item"
msgid "Save today's picture to local disk"
msgstr "Bu günün şəklini yerli diskdə saxlamaq"

#: package/contents/ui/WallpaperPreview.qml:59
#, kde-format
msgctxt ""
"@action:inmenu wallpaper preview menu, will open the website of the wallpaper"
msgid "Open Link in Browser…"
msgstr "Keçidi veb-bələdçidə açın..."

#: package/contents/ui/WallpaperPreview.qml:62
#, kde-format
msgctxt "@info:whatsthis for a menu item"
msgid "Open the website of today's picture in the default browser"
msgstr "Bu günün şəkli veb-səhifəsini standart veb-bələdçidə açın"

#: plugins/potdbackend.cpp:221
#, kde-format
msgctxt "@title:window"
msgid "Save Today's Picture"
msgstr "Bu günün şəklini saxlamyın"

#: plugins/potdbackend.cpp:223
#, kde-format
msgctxt "@label:listbox Template for file dialog"
msgid "JPEG image (*.jpeg *.jpg *.jpe)"
msgstr "JPEG şəkilləri (*.jpeg *.jpg *.jpe)"

#: plugins/potdbackend.cpp:240
#, kde-format
msgctxt "@info:status after a save action"
msgid "The image was not saved."
msgstr "Şəkil saxlanılmadı."

#: plugins/potdbackend.cpp:246
#, kde-format
msgctxt "@info:status after a save action %1 file path %2 basename"
msgid "The image was saved as <a href=\"%1\">%2</a>"
msgstr "Şəkil <a href=\"%1\">%2</a> kimi saxlanıldı"
