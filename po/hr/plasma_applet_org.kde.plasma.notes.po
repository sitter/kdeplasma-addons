# Translation of plasma_applet_notes to Croatian
#
# Žarko <zarko.pintar@gmail.com>, 2010.
# Marko Dimjasevic <marko@dimjasevic.net>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-06-30 00:46+0000\n"
"PO-Revision-Date: 2010-07-23 12:07+0200\n"
"Last-Translator: Marko Dimjasevic <marko@dimjasevic.net>\n"
"Language-Team: Croatian <kde-croatia-list@lists.sourceforge.net>\n"
"Language: hr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.0\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: package/contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "Appearance"
msgstr ""

#: package/contents/ui/configAppearance.qml:30
#, kde-format
msgid "%1pt"
msgstr ""

#: package/contents/ui/configAppearance.qml:36
#, fuzzy, kde-format
#| msgid "Use custom font size:"
msgid "Text font size:"
msgstr "Koristi prilagođenu veličinu pisma:"

#: package/contents/ui/configAppearance.qml:41
#, kde-format
msgid "Background color"
msgstr ""

#: package/contents/ui/configAppearance.qml:77
#, kde-format
msgid "A white sticky note"
msgstr ""

#: package/contents/ui/configAppearance.qml:78
#, kde-format
msgid "A black sticky note"
msgstr ""

#: package/contents/ui/configAppearance.qml:79
#, kde-format
msgid "A red sticky note"
msgstr ""

#: package/contents/ui/configAppearance.qml:80
#, kde-format
msgid "An orange sticky note"
msgstr ""

#: package/contents/ui/configAppearance.qml:81
#, kde-format
msgid "A yellow sticky note"
msgstr ""

#: package/contents/ui/configAppearance.qml:82
#, kde-format
msgid "A green sticky note"
msgstr ""

#: package/contents/ui/configAppearance.qml:83
#, kde-format
msgid "A blue sticky note"
msgstr ""

#: package/contents/ui/configAppearance.qml:84
#, kde-format
msgid "A pink sticky note"
msgstr ""

#: package/contents/ui/configAppearance.qml:85
#, kde-format
msgid "A translucent sticky note"
msgstr ""

#: package/contents/ui/configAppearance.qml:86
#, kde-format
msgid "A translucent sticky note with light text"
msgstr ""

#: package/contents/ui/main.qml:246
#, kde-format
msgid "Undo"
msgstr ""

#: package/contents/ui/main.qml:254
#, kde-format
msgid "Redo"
msgstr ""

#: package/contents/ui/main.qml:264
#, kde-format
msgid "Cut"
msgstr ""

#: package/contents/ui/main.qml:272
#, kde-format
msgid "Copy"
msgstr ""

#: package/contents/ui/main.qml:280
#, kde-format
msgid "Paste Without Formatting"
msgstr ""

#: package/contents/ui/main.qml:286
#, kde-format
msgid "Paste"
msgstr ""

#: package/contents/ui/main.qml:295
#, kde-format
msgid "Delete"
msgstr ""

#: package/contents/ui/main.qml:302
#, kde-format
msgid "Clear"
msgstr ""

#: package/contents/ui/main.qml:312
#, kde-format
msgid "Select All"
msgstr ""

#: package/contents/ui/main.qml:440
#, fuzzy, kde-format
#| msgid "Bold"
msgctxt "@info:tooltip"
msgid "Bold"
msgstr "Podebljano"

#: package/contents/ui/main.qml:452
#, fuzzy, kde-format
#| msgid "Italic"
msgctxt "@info:tooltip"
msgid "Italic"
msgstr "Kurziv"

#: package/contents/ui/main.qml:464
#, fuzzy, kde-format
#| msgid "Underline"
msgctxt "@info:tooltip"
msgid "Underline"
msgstr "Podvučeno"

#: package/contents/ui/main.qml:476
#, fuzzy, kde-format
#| msgid "StrikeOut"
msgctxt "@info:tooltip"
msgid "Strikethrough"
msgstr "Precrtano"

#: package/contents/ui/main.qml:550
#, kde-format
msgid "Discard this note?"
msgstr ""

#: package/contents/ui/main.qml:551
#, kde-format
msgid "Are you sure you want to discard this note?"
msgstr ""

#: package/contents/ui/main.qml:564
#, fuzzy, kde-format
#| msgid "White"
msgctxt "@item:inmenu"
msgid "White"
msgstr "Bijela"

#: package/contents/ui/main.qml:565
#, fuzzy, kde-format
#| msgid "Black"
msgctxt "@item:inmenu"
msgid "Black"
msgstr "Crna"

#: package/contents/ui/main.qml:566
#, fuzzy, kde-format
#| msgid "Red"
msgctxt "@item:inmenu"
msgid "Red"
msgstr "Crvena"

#: package/contents/ui/main.qml:567
#, fuzzy, kde-format
#| msgid "Orange"
msgctxt "@item:inmenu"
msgid "Orange"
msgstr "Narančasta"

#: package/contents/ui/main.qml:568
#, fuzzy, kde-format
#| msgid "Yellow"
msgctxt "@item:inmenu"
msgid "Yellow"
msgstr "Žuta"

#: package/contents/ui/main.qml:569
#, fuzzy, kde-format
#| msgid "Green"
msgctxt "@item:inmenu"
msgid "Green"
msgstr "Zelena"

#: package/contents/ui/main.qml:570
#, fuzzy, kde-format
#| msgid "Blue"
msgctxt "@item:inmenu"
msgid "Blue"
msgstr "Plava"

#: package/contents/ui/main.qml:571
#, fuzzy, kde-format
#| msgid "Pink"
msgctxt "@item:inmenu"
msgid "Pink"
msgstr "Ružičasta"

#: package/contents/ui/main.qml:572
#, fuzzy, kde-format
#| msgid "Translucent"
msgctxt "@item:inmenu"
msgid "Translucent"
msgstr "Prozirna"

#: package/contents/ui/main.qml:573
#, fuzzy, kde-format
#| msgid "Translucent"
msgctxt "@item:inmenu"
msgid "Translucent Light"
msgstr "Prozirna"

#, fuzzy
#~| msgid "Justify center"
#~ msgid "Align center"
#~ msgstr "Poravnaj u sredinu"

#, fuzzy
#~| msgid "Justify"
#~ msgid "Justified"
#~ msgstr "Poravnaj"

#~ msgid "Font"
#~ msgstr "Pismo"

#~ msgid "Style:"
#~ msgstr "Stil:"

#~ msgid "&Bold"
#~ msgstr "Pode&bljano"

#~ msgid "&Italic"
#~ msgstr "Kurz&iv"

#~ msgid "Size:"
#~ msgstr "Veličina:"

#~ msgid "Scale font size by:"
#~ msgstr "Rastegni veličinu pisma za:"

#~ msgid "%"
#~ msgstr "%"

#~ msgid "Color:"
#~ msgstr "Boja:"

#~ msgid "Use theme color"
#~ msgstr "Koristi boju iz teme"

#~ msgid "Use custom color:"
#~ msgstr "Koristi prilagođenu boju:"

#~ msgid "Active line highlight color:"
#~ msgstr "Boja osvjetljenja aktivne linije:"

#~ msgid "Use no color"
#~ msgstr "Ne koristi boju"

#~ msgid "Theme"
#~ msgstr "Tema"

#~ msgid "Notes color:"
#~ msgstr "Boja bilješke:"

#~ msgid "Spell Check"
#~ msgstr "Provjera pravopisa"

#~ msgid "Enable spell check:"
#~ msgstr "Omogući provjeru pravopisa:"

#~ msgid "Notes Color"
#~ msgstr "Boja bilješke"

#~ msgid "General"
#~ msgstr "Opće"

#~ msgid "Formatting"
#~ msgstr "Oblikovanje"

#~ msgid "Unable to open file"
#~ msgstr "Nije moguće otvoriti datoteku"
