# translation of plasma_packagestructure_comic.po to Français
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Joëlle Cornavin <jcornavi@club-internet.fr>, 2009.
# xavier <xavier.besnard@neuf.fr>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma_packagestructure_comic\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-08-25 00:19+0000\n"
"PO-Revision-Date: 2013-05-16 16:14+0200\n"
"Last-Translator: xavier <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-i18n-doc@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 1.5\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: comic_package.cpp:20
#, kde-format
msgid "Images"
msgstr "Images"

#: comic_package.cpp:25
#, kde-format
msgid "Executable Scripts"
msgstr "Scripts exécutables"

#: comic_package.cpp:29
#, kde-format
msgid "Main Script File"
msgstr "Fichier principal de script"
