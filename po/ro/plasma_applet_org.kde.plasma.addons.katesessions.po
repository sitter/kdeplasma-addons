# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Sergiu Bivol <sergiu@cip.md>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-17 00:48+0000\n"
"PO-Revision-Date: 2014-10-18 00:39+0300\n"
"Last-Translator: Sergiu Bivol <sergiu@ase.md>\n"
"Language-Team: Romanian <kde-i18n-ro@kde.org>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"

#: contents/ui/KateSessionsItemDelegate.qml:91
#, kde-format
msgid "Session name"
msgstr "Denumirea sesiunii"

#: contents/ui/KateSessionsItemDelegate.qml:102
#, kde-format
msgid "Create new session and start Kate"
msgstr "Creează sesiune nouă și pornește Kate"

#: contents/ui/KateSessionsItemDelegate.qml:111
#, kde-format
msgid "Cancel session creation"
msgstr "Anulează crearea sesiunii"

#: contents/ui/main.qml:29
#, kde-format
msgid "Kate Sessions"
msgstr "Sesiuni Kate"

#: contents/ui/main.qml:38
#, fuzzy, kde-format
#| msgid "Search"
msgid "Search…"
msgstr "Caută"

#~ msgid "Delete session"
#~ msgstr "Șterge sesiunea"

#~ msgid "Start Kate (no arguments)"
#~ msgstr "Pornește Kate (fără argumente)"

#~ msgid "New Kate Session"
#~ msgstr "Sesiune Kate nouă"

#~ msgid "New Anonymous Session"
#~ msgstr "Sesiune nouă anonimă"
