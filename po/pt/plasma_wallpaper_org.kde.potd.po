msgid ""
msgstr ""
"Project-Id-Version: plasma_wallpaper_org\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-28 00:51+0000\n"
"PO-Revision-Date: 2022-08-15 01:14+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: Tumblr Ultra jpg jpe Tabliss\n"

#: package/contents/ui/config.qml:48
#, kde-format
msgctxt "@label:listbox"
msgid "Provider:"
msgstr "Fornecedor:"

#: package/contents/ui/config.qml:66
#, kde-format
msgctxt "@option:check"
msgid "Update when using metered network connection"
msgstr "Actualizar ao usar uma ligação de rede com tráfego limitado"

#: package/contents/ui/config.qml:76
#, kde-format
msgctxt "@label:listbox"
msgid "Category:"
msgstr "Categoria:"

#: package/contents/ui/config.qml:80
#, kde-format
msgctxt "@item:inlistbox"
msgid "All"
msgstr "Tudo"

#: package/contents/ui/config.qml:84
#, kde-format
msgctxt "@item:inlistbox"
msgid "1080p"
msgstr "1080p"

#: package/contents/ui/config.qml:88
#, kde-format
msgctxt "@item:inlistbox"
msgid "4K"
msgstr "4K"

#: package/contents/ui/config.qml:92
#, kde-format
msgctxt "@item:inlistbox"
msgid "Ultra Wide"
msgstr "Ultra Panorâmico"

#: package/contents/ui/config.qml:96
#, kde-format
msgctxt "@item:inlistbox"
msgid "Background"
msgstr "Fundo"

#: package/contents/ui/config.qml:100
#, kde-format
msgctxt "@item:inlistbox"
msgid "Lock Screen"
msgstr "Bloquear o Ecrã"

#: package/contents/ui/config.qml:104
#, kde-format
msgctxt "@item:inlistbox"
msgid "Nature"
msgstr "Natureza"

#: package/contents/ui/config.qml:108
#, kde-format
msgctxt "@item:inlistbox"
msgid "Tumblr"
msgstr "Tumblr"

#: package/contents/ui/config.qml:112
#, kde-format
msgctxt "@item:inlistbox"
msgid "Black"
msgstr "Preto"

#: package/contents/ui/config.qml:116
#, kde-format
msgctxt "@item:inlistbox"
msgid "Flower"
msgstr "Flor"

#: package/contents/ui/config.qml:120
#, kde-format
msgctxt "@item:inlistbox"
msgid "Funny"
msgstr "Engraçado"

#: package/contents/ui/config.qml:124
#, kde-format
msgctxt "@item:inlistbox"
msgid "Cute"
msgstr "Bonito"

#: package/contents/ui/config.qml:128
#, kde-format
msgctxt "@item:inlistbox"
msgid "Cool"
msgstr "Com Estilo"

#: package/contents/ui/config.qml:132
#, kde-format
msgctxt "@item:inlistbox"
msgid "Fall"
msgstr "Outono"

#: package/contents/ui/config.qml:136
#, kde-format
msgctxt "@item:inlistbox"
msgid "Love"
msgstr "Amor"

#: package/contents/ui/config.qml:140
#, kde-format
msgctxt "@item:inlistbox"
msgid "Design"
msgstr "Desenho"

#: package/contents/ui/config.qml:144
#, kde-format
msgctxt "@item:inlistbox"
msgid "Christmas"
msgstr "Natal"

#: package/contents/ui/config.qml:148
#, kde-format
msgctxt "@item:inlistbox"
msgid "Travel"
msgstr "Viagens"

#: package/contents/ui/config.qml:152
#, kde-format
msgctxt "@item:inlistbox"
msgid "Beach"
msgstr "Praia"

#: package/contents/ui/config.qml:156
#, kde-format
msgctxt "@item:inlistbox"
msgid "Car"
msgstr "Carro"

#: package/contents/ui/config.qml:160
#, kde-format
msgctxt "@item:inlistbox"
msgid "Sports"
msgstr "Desportos"

#: package/contents/ui/config.qml:164
#, kde-format
msgctxt "@item:inlistbox"
msgid "Animal"
msgstr "Animal"

#: package/contents/ui/config.qml:168
#, kde-format
msgctxt "@item:inlistbox"
msgid "People"
msgstr "Pessoas"

#: package/contents/ui/config.qml:172
#, kde-format
msgctxt "@item:inlistbox"
msgid "Music"
msgstr "Música"

#: package/contents/ui/config.qml:176
#, kde-format
msgctxt "@item:inlistbox"
msgid "Summer"
msgstr "Verão"

#: package/contents/ui/config.qml:180
#, kde-format
msgctxt "@item:inlistbox"
msgid "Galaxy"
msgstr "Galáxia"

#: package/contents/ui/config.qml:184
#, kde-format
msgctxt "@item:inlistbox"
msgid "Tabliss"
msgstr "Tabliss"

#: package/contents/ui/config.qml:214
#, kde-format
msgctxt "@label:listbox"
msgid "Positioning:"
msgstr "Posicionamento:"

#: package/contents/ui/config.qml:217
#, kde-format
msgctxt "@item:inlistbox"
msgid "Scaled and Cropped"
msgstr "Escalado e Recortado"

#: package/contents/ui/config.qml:221
#, kde-format
msgctxt "@item:inlistbox"
msgid "Scaled"
msgstr "Escalado"

#: package/contents/ui/config.qml:225
#, kde-format
msgctxt "@item:inlistbox"
msgid "Scaled, Keep Proportions"
msgstr "Escalado Proporcionalmente"

#: package/contents/ui/config.qml:229
#, kde-format
msgctxt "@item:inlistbox"
msgid "Centered"
msgstr "Centrado"

#: package/contents/ui/config.qml:233
#, kde-format
msgctxt "@item:inlistbox"
msgid "Tiled"
msgstr "Mosaico"

#: package/contents/ui/config.qml:254
#, kde-format
msgctxt "@label:chooser"
msgid "Background color:"
msgstr "Cor de fundo:"

#: package/contents/ui/config.qml:255
#, kde-format
msgctxt "@title:window"
msgid "Select Background Color"
msgstr "Seleccionar a Cor de Fundo"

#: package/contents/ui/config.qml:266
#, kde-format
msgctxt "@label"
msgid "Today's picture:"
msgstr "Imagem de hoje:"

#: package/contents/ui/config.qml:283
#, kde-format
msgctxt "@label"
msgid "Title:"
msgstr "Título:"

#: package/contents/ui/config.qml:303
#, kde-format
msgctxt "@label"
msgid "Author:"
msgstr "Autor:"

#: package/contents/ui/config.qml:329
#, kde-format
msgctxt "@action:button"
msgid "Open Containing Folder"
msgstr "Abrir a Pasta Respectiva"

#: package/contents/ui/config.qml:333
#, kde-format
msgctxt "@info:whatsthis for a button"
msgid "Open the destination folder where the wallpaper image was saved."
msgstr "Abre a pasta de destino onde foi gravada a imagem do papel de parede."

#: package/contents/ui/WallpaperDelegate.qml:142
#, kde-format
msgctxt "@info:whatsthis"
msgid "Today's picture"
msgstr "Imagem de hoje"

#: package/contents/ui/WallpaperDelegate.qml:143
#, kde-format
msgctxt "@info:whatsthis"
msgid "Loading"
msgstr "A carregar"

#: package/contents/ui/WallpaperDelegate.qml:144
#, kde-format
msgctxt "@info:whatsthis"
msgid "Unavailable"
msgstr "Indisponível"

#: package/contents/ui/WallpaperDelegate.qml:145
#, kde-format
msgctxt "@info:whatsthis for an image %1 title %2 author"
msgid "%1 Author: %2. Right-click on the image to see more actions."
msgstr ""
"%1 Autor: %2. Carregue com o botão direito na imagem para ver mais acções."

#: package/contents/ui/WallpaperDelegate.qml:146
#, kde-format
msgctxt "@info:whatsthis"
msgid "The wallpaper is being fetched from the Internet."
msgstr "O papel de parede está a ser transferido da Internet."

#: package/contents/ui/WallpaperDelegate.qml:147
#, kde-format
msgctxt "@info:whatsthis"
msgid "Failed to fetch the wallpaper from the Internet."
msgstr "Não foi possível obter o papel de parede da Internet."

#: package/contents/ui/WallpaperPreview.qml:50
#, kde-format
msgctxt "@action:inmenu wallpaper preview menu"
msgid "Save Image as…"
msgstr "Gravar a Imagem como…"

#: package/contents/ui/WallpaperPreview.qml:53
#, kde-format
msgctxt "@info:whatsthis for a button and a menu item"
msgid "Save today's picture to local disk"
msgstr "Gravar a imagem de hoje no disco local"

#: package/contents/ui/WallpaperPreview.qml:59
#, kde-format
msgctxt ""
"@action:inmenu wallpaper preview menu, will open the website of the wallpaper"
msgid "Open Link in Browser…"
msgstr "Abrir a Ligação no Navegador…"

#: package/contents/ui/WallpaperPreview.qml:62
#, kde-format
msgctxt "@info:whatsthis for a menu item"
msgid "Open the website of today's picture in the default browser"
msgstr "Abre a página Web da imagem de hoje no navegador predefinido"

#: plugins/potdbackend.cpp:221
#, kde-format
msgctxt "@title:window"
msgid "Save Today's Picture"
msgstr "Gravar a Imagem de Hoje"

#: plugins/potdbackend.cpp:223
#, kde-format
msgctxt "@label:listbox Template for file dialog"
msgid "JPEG image (*.jpeg *.jpg *.jpe)"
msgstr "Imagem JPEG (*.jpeg *.jpg *.jpe)"

#: plugins/potdbackend.cpp:240
#, kde-format
msgctxt "@info:status after a save action"
msgid "The image was not saved."
msgstr "A imagem não foi gravada."

#: plugins/potdbackend.cpp:246
#, kde-format
msgctxt "@info:status after a save action %1 file path %2 basename"
msgid "The image was saved as <a href=\"%1\">%2</a>"
msgstr "A imagem foi gravada como <a href=\"%1\">%2</a>"
