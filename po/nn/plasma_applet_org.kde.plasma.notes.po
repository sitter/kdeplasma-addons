# Translation of plasma_applet_org.kde.plasma.notes to Norwegian Nynorsk
#
# Karl Ove Hufthammer <karl@huftis.org>, 2007, 2008, 2015, 2018, 2019, 2021.
# Eirik U. Birkeland <eirbir@gmail.com>, 2008, 2009.
# Øystein Steffensen-Alværvik <oysteins.omsetting@protonmail.com>, 2018.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_notes\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-06-30 00:46+0000\n"
"PO-Revision-Date: 2021-09-19 13:17+0200\n"
"Last-Translator: Karl Ove Hufthammer <karl@huftis.org>\n"
"Language-Team: Norwegian Nynorsk <l10n-no@lister.huftis.org>\n"
"Language: nn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.11.70\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: package/contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "Appearance"
msgstr "Utsjånad"

#: package/contents/ui/configAppearance.qml:30
#, kde-format
msgid "%1pt"
msgstr "%1 pt"

#: package/contents/ui/configAppearance.qml:36
#, kde-format
msgid "Text font size:"
msgstr "Skriftstorleik:"

#: package/contents/ui/configAppearance.qml:41
#, kde-format
msgid "Background color"
msgstr "Bakgrunnsfarge"

#: package/contents/ui/configAppearance.qml:77
#, kde-format
msgid "A white sticky note"
msgstr "Ein kvit notatlapp"

#: package/contents/ui/configAppearance.qml:78
#, kde-format
msgid "A black sticky note"
msgstr "Ein svart notatlapp"

#: package/contents/ui/configAppearance.qml:79
#, kde-format
msgid "A red sticky note"
msgstr "Ein raud notatlapp"

#: package/contents/ui/configAppearance.qml:80
#, kde-format
msgid "An orange sticky note"
msgstr "Ein oransje notatlapp"

#: package/contents/ui/configAppearance.qml:81
#, kde-format
msgid "A yellow sticky note"
msgstr "Ein gul notatlapp"

#: package/contents/ui/configAppearance.qml:82
#, kde-format
msgid "A green sticky note"
msgstr "Ein grøn notatlapp"

#: package/contents/ui/configAppearance.qml:83
#, kde-format
msgid "A blue sticky note"
msgstr "Ein blå notatlapp"

#: package/contents/ui/configAppearance.qml:84
#, kde-format
msgid "A pink sticky note"
msgstr "Ein rosa notatlapp"

#: package/contents/ui/configAppearance.qml:85
#, kde-format
msgid "A translucent sticky note"
msgstr "Ein gjennomsiktig notatlapp"

#: package/contents/ui/configAppearance.qml:86
#, kde-format
msgid "A translucent sticky note with light text"
msgstr "Ein gjennomsiktig notatlapp med lys tekst"

#: package/contents/ui/main.qml:246
#, kde-format
msgid "Undo"
msgstr "Angra"

#: package/contents/ui/main.qml:254
#, kde-format
msgid "Redo"
msgstr "Gjer om"

#: package/contents/ui/main.qml:264
#, kde-format
msgid "Cut"
msgstr "Klipp ut"

#: package/contents/ui/main.qml:272
#, kde-format
msgid "Copy"
msgstr "Kopier"

#: package/contents/ui/main.qml:280
#, kde-format
msgid "Paste Without Formatting"
msgstr "Lim inn utan formatering"

#: package/contents/ui/main.qml:286
#, kde-format
msgid "Paste"
msgstr "Lim inn"

#: package/contents/ui/main.qml:295
#, kde-format
msgid "Delete"
msgstr "Slett"

#: package/contents/ui/main.qml:302
#, kde-format
msgid "Clear"
msgstr "Tøm"

#: package/contents/ui/main.qml:312
#, kde-format
msgid "Select All"
msgstr "Merk alt"

#: package/contents/ui/main.qml:440
#, kde-format
msgctxt "@info:tooltip"
msgid "Bold"
msgstr "Halvfeit"

#: package/contents/ui/main.qml:452
#, kde-format
msgctxt "@info:tooltip"
msgid "Italic"
msgstr "Kursiv"

#: package/contents/ui/main.qml:464
#, kde-format
msgctxt "@info:tooltip"
msgid "Underline"
msgstr "Understreking"

#: package/contents/ui/main.qml:476
#, kde-format
msgctxt "@info:tooltip"
msgid "Strikethrough"
msgstr "Gjennomstreking"

#: package/contents/ui/main.qml:550
#, kde-format
msgid "Discard this note?"
msgstr "Vil du fjerna dette notatet?"

#: package/contents/ui/main.qml:551
#, kde-format
msgid "Are you sure you want to discard this note?"
msgstr "Er du sikker på at du vil fjerna dette notatet?"

#: package/contents/ui/main.qml:564
#, kde-format
msgctxt "@item:inmenu"
msgid "White"
msgstr "Kvit"

#: package/contents/ui/main.qml:565
#, kde-format
msgctxt "@item:inmenu"
msgid "Black"
msgstr "Svart"

#: package/contents/ui/main.qml:566
#, kde-format
msgctxt "@item:inmenu"
msgid "Red"
msgstr "Raud"

#: package/contents/ui/main.qml:567
#, kde-format
msgctxt "@item:inmenu"
msgid "Orange"
msgstr "Oransje"

#: package/contents/ui/main.qml:568
#, kde-format
msgctxt "@item:inmenu"
msgid "Yellow"
msgstr "Gul"

#: package/contents/ui/main.qml:569
#, kde-format
msgctxt "@item:inmenu"
msgid "Green"
msgstr "Grøn"

#: package/contents/ui/main.qml:570
#, kde-format
msgctxt "@item:inmenu"
msgid "Blue"
msgstr "Blå"

#: package/contents/ui/main.qml:571
#, kde-format
msgctxt "@item:inmenu"
msgid "Pink"
msgstr "Rosa"

#: package/contents/ui/main.qml:572
#, kde-format
msgctxt "@item:inmenu"
msgid "Translucent"
msgstr "Gjennomsiktig"

#: package/contents/ui/main.qml:573
#, kde-format
msgctxt "@item:inmenu"
msgid "Translucent Light"
msgstr "Gjennomsiktig lys"
