# Translation of plasma_applet_org.kde.plasma.nightcolorcontrol to Norwegian Nynorsk
#
# Karl Ove Hufthammer <karl@huftis.org>, 2020, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kdeplasma-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-12-13 00:44+0000\n"
"PO-Revision-Date: 2022-03-03 21:09+0100\n"
"Last-Translator: Karl Ove Hufthammer <karl@huftis.org>\n"
"Language-Team: Norwegian Nynorsk <l10n-no@lister.huftis.org>\n"
"Language: nn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.2\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: package/contents/ui/main.qml:31
#, kde-format
msgid "Night Color Control"
msgstr "Styring av kveldsfargar"

#: package/contents/ui/main.qml:34
#, kde-format
msgid "Night Color is inhibited"
msgstr "Kveldsfargar er blokkerte"

#: package/contents/ui/main.qml:37
#, kde-format
msgid "Night Color is unavailable"
msgstr "Kveldsfargar er ikkje tilgjengelege"

#: package/contents/ui/main.qml:40
#, kde-format
msgid "Night Color is disabled"
msgstr "Kveldsfargar er av"

#: package/contents/ui/main.qml:43
#, kde-format
msgid "Night Color is not running"
msgstr "Kveldsfargar køyrer ikkje"

#: package/contents/ui/main.qml:45
#, kde-format
msgid "Night Color is active (%1K)"
msgstr "Kveldsfargar er på (%1 K)"

#: package/contents/ui/main.qml:94
#, kde-format
msgid "Configure Night Color…"
msgstr "Set opp kveldsfargar …"
